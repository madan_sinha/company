package com.bs.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bs.entities.Company;

import java.util.List;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

	@Query("select  c from Company c where c.id = ?1")
	public Company getCompanyById(Long id);

	@Query(nativeQuery = true, value = "Select c.* from company c join company_supplier_map csm on csm.company_id = c.id where csm.supplier_id = ?1")
    List<Company> findCompaniesBySupplierIds(Long supplierId);
}
