package com.bs.repositories;

import com.bs.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query(nativeQuery = true, value = "select max(id) from user")
    public Long sequenceForUser();

    @Query(nativeQuery = true, value = "select u.* from user u join work_group_user_map wum on wum.user_id = u.id where wum.work_group_id = ?1")
    List<User> findAllByWorkGroups(Long workgroupId);

    @Query(nativeQuery = true, value = "select u.* from user u join work_group_user_map wum on wum.user_id = u.id join work_group wg on wum.work_group_id = wg.id join company c on c.id = wg.company_id where c.id = ?1")
    List<User> findAllUsersByCompanyId(Long companyId);
}
