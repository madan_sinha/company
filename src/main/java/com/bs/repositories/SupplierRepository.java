package com.bs.repositories;

import com.bs.entities.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupplierRepository extends JpaRepository<Supplier, Long> {

    @Query(nativeQuery = true, value = "Select s.* from supplier s join company_supplier_map csm on csm.supplier_id = s.id where csm.company_id = ?1")
    List<Supplier> findSuppliersByCompanyId(Long companyId);

    @Query(nativeQuery = true, value = "select max(id) from supplier")
    Long sequenceForSupplier();
}
