package com.bs.repositories;

import com.bs.entities.WorkGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorkGroupRepository extends JpaRepository<WorkGroup, Long> {

    List<WorkGroup> findAllByCompanyId(Long companyId);
}
