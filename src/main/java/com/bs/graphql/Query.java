package com.bs.graphql;

import com.bs.dtos.CompanyDto;
import com.bs.dtos.SupplierDto;
import com.bs.dtos.UserDto;
import com.bs.dtos.WorkGroupDto;
import com.bs.services.CompanyService;
import com.bs.services.SupplierService;
import com.bs.services.UserService;
import com.bs.services.WorkGroupService;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Query implements GraphQLQueryResolver {

    @Autowired
    CompanyService companyService;

    @Autowired
    WorkGroupService workGroupService;

    @Autowired
    UserService userService;

    @Autowired
    SupplierService supplierService;

    public List<CompanyDto> findAllCompanies() {
        return companyService.findAll();
    }

    public CompanyDto companyById(Long companyId) {
        return companyService.findCompany(companyId);
    }

    public List<CompanyDto> companiesBySupplierId(Long supplierId) {
        return companyService.findCompaniesBySupplierIds(supplierId);
    }

    public WorkGroupDto workgroupById(Long id) {
        return workGroupService.findWorkGroup(id);
    }

    public List<WorkGroupDto> workgroupByCompanyId(Long companyId) {
        return workGroupService.getWorkGroupsByCompanyId(companyId);
    }

    public UserDto userById(Long id) {
        return userService.getUserById(id);
    }

    public List<UserDto> listUsersByWorkGroupId(Long workgroupId) {
        return userService.getUsersByWorkGroupId(workgroupId);
    }

    public List<UserDto> listUsersByCompanyId(Long companyId) {
        return userService.getUsersByCompanyId(companyId);
    }

    public SupplierDto supplierById(Long supplierId) {
        return supplierService.findSupplierById(supplierId);
    }

    public List<SupplierDto> supplierByCompanyId(Long companyId) {
        return supplierService.findSuppliersByCompanyId(companyId);
    }
}
