package com.bs.graphql;

import com.bs.dtos.CompanyDto;
import com.bs.dtos.SupplierDto;
import com.bs.dtos.UserDto;
import com.bs.dtos.WorkGroupDto;
import com.bs.services.CompanyService;
import com.bs.services.SupplierService;
import com.bs.services.UserService;
import com.bs.services.WorkGroupService;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Mutation implements GraphQLMutationResolver {

    @Autowired
    CompanyService companyService;

    @Autowired
    WorkGroupService workGroupService;

    @Autowired
    UserService userService;

    @Autowired
    SupplierService supplierService;

    public Integer createCompany(String companyName, String companyCode, String companyDescription) {
        CompanyDto companyDto = new CompanyDto(companyCode, companyName, companyDescription);
        return companyService.createCompany(companyDto).intValue();
    }

    public Integer createWorkGroup(String groupName, String code, String description, Long companyId) {
        WorkGroupDto workGroupDto = new WorkGroupDto(groupName, code, description, companyId);
        return workGroupService.createWorkGroup(workGroupDto).intValue();
    }

    public Integer createUser(String name, String email, List<Long> workGroupsIds) {
        UserDto userDto = new UserDto(name, email, workGroupsIds);
        return userService.createUser(userDto).intValue();
    }

    public Integer createSupplier(String name, String address, String gis, List<Long> companyIds) {
        SupplierDto supplierDto = new SupplierDto(name, address, gis, companyIds);
        return supplierService.createSupplier(supplierDto).intValue();
    }

}
