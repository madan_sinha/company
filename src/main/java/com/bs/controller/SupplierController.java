package com.bs.controller;

import com.bs.dtos.SupplierDto;
import com.bs.services.SupplierService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("supplier")
public class SupplierController {
    Logger logger = LoggerFactory.getLogger(SupplierController.class);

    @Autowired
    private SupplierService supplierService;

    @PostMapping("/create")
    public ResponseEntity<Long> create(@RequestBody final SupplierDto supplierDto) {
        return ResponseEntity.status(HttpStatus.OK).body(supplierService.createSupplier(supplierDto));
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<SupplierDto> getSupplierById(@PathVariable Long id) {
        logger.info("SupplierController getCompanyById: {}", id);
        return ResponseEntity.status(HttpStatus.OK).body(supplierService.findSupplierById(id));
    }

    @GetMapping("/getByCompany/{companyId}")
    public ResponseEntity<List<SupplierDto>> getSuppliersByCompanyId(@PathVariable("companyId") Long companyId) {
        logger.info("SupplierController getByCompany : {companyId}", companyId);
        return ResponseEntity.status(HttpStatus.OK).body(supplierService.findSuppliersByCompanyId(companyId));
    }
}
