package com.bs.controller;

import com.bs.dtos.WorkGroupDto;
import com.bs.services.WorkGroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("workgroup")
public class WorkGroupController {

    Logger logger = LoggerFactory.getLogger(WorkGroupController.class);

    @Autowired
    private WorkGroupService workGroupService;

    @GetMapping("/get/{workGroupId}")
    public ResponseEntity<WorkGroupDto> getWorkGroup(@PathVariable("workGroupId") Long workGroupId) {
        WorkGroupDto workGroupDto = workGroupService.findWorkGroup(workGroupId);
        return ResponseEntity.status(HttpStatus.OK).body(workGroupDto);
    }

    @PostMapping("/create")
    public  ResponseEntity<Long> createWorkGroup(@RequestBody final WorkGroupDto workGroupDto ) {
        Long workGroupId = 0l;
        try {
            workGroupId = workGroupService.createWorkGroup(workGroupDto);
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(workGroupId);
        }

        return ResponseEntity.status(HttpStatus.OK).body(workGroupId);
    }

    @GetMapping("/getByCompany/{companyId}")
    public ResponseEntity<List<WorkGroupDto>> getWorkGroupsByCompanyId(@PathVariable("companyId") Long companyId) {
        logger.info("WorkGroupController getUsersByWorkGroupId: {}", companyId);

        List<WorkGroupDto> userDtoList = workGroupService.getWorkGroupsByCompanyId(companyId);
        return ResponseEntity.status(HttpStatus.OK).body(userDtoList);
    }

}
