package com.bs.controller;

import com.bs.dtos.CompanyDto;
import com.bs.dtos.UserDto;
import com.bs.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("user")
public class UserController {

    Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private UserService userService;

    @PostMapping("/create")
    public ResponseEntity<Long> createUser(@RequestBody final UserDto userDto ) {
        Long userId = 0l;
        try {
            userId = userService.createUser(userDto);
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(userId);
        }

        return ResponseEntity.status(HttpStatus.OK).body(userId);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<UserDto> getUserById(@PathVariable("id") Long id) {
        logger.info("UserController getCompanyById: {}", id);
        return ResponseEntity.status(HttpStatus.OK).body(userService.getUserById(id));
    }

    @GetMapping("/getByWorkGroup/{workgroupId}")
    public ResponseEntity<List<UserDto>> getUsersByWorkGroupId(@PathVariable("workgroupId") Long workgroupId) {
        logger.info("UserController getUsersByWorkGroupId: {}", workgroupId);

        List<UserDto> userDtoList = userService.getUsersByWorkGroupId(workgroupId);
        return ResponseEntity.status(HttpStatus.OK).body(userDtoList);
    }

    @GetMapping("/getByCompany/{companyId}")
    public ResponseEntity<List<UserDto>> getUsersByCompanyId(@PathVariable("companyId") Long companyId) {
        logger.info("UserController getUsersByCompanyId: {}", companyId);

        List<UserDto> userDtoList = userService.getUsersByCompanyId(companyId);
        return ResponseEntity.status(HttpStatus.OK).body(userDtoList);
    }
}
