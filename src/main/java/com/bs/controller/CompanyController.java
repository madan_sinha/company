package com.bs.controller;

import java.util.List;

import com.bs.dtos.CompanyDto;
import com.bs.kafka.MessageProducer;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.bs.services.CompanyService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("company")
public class CompanyController {

	Logger logger = LoggerFactory.getLogger(CompanyController.class);

	@Autowired
	private CompanyService companyService;

	@Autowired
	private MessageProducer messageProducer;

	@Autowired
	private Gson gson;

	@PostMapping("/create")
	public ResponseEntity<Long> create(@RequestBody final CompanyDto companyDto) {
		return ResponseEntity.status(HttpStatus.OK).body(companyService.createCompany(companyDto));
	}

	@GetMapping("/get/{id}")
	public ResponseEntity<CompanyDto> getCompanyById(@PathVariable Long id) {
		logger.info("CompanyController getCompanyById: {}", id);
		return ResponseEntity.status(HttpStatus.OK).body(companyService.findCompany(id));
	}

	@GetMapping("/all")
	public ResponseEntity<List<CompanyDto>> findAll() {
		return ResponseEntity.status(HttpStatus.OK).body(companyService.findAll());
	}

	@PostMapping("/createByKafka")
	public ResponseEntity<String> createByKafka(@RequestBody final CompanyDto companyDto) {
		//String jsonMessage = gson.toJson(companyDto);
		messageProducer.sendMessage(companyDto);
		return ResponseEntity.status(HttpStatus.OK).body("Message Sent successfully to Kafka");
	}

	@GetMapping("/getBySupplier/{supplierId}")
	public ResponseEntity<List<CompanyDto>> getCompanyBySupplierId(@PathVariable("supplierId") Long supplierId) {
		logger.info("CompanyController getCompanyById: {}", supplierId);
		return ResponseEntity.status(HttpStatus.OK).body(companyService.findCompaniesBySupplierIds(supplierId));
	}
}
