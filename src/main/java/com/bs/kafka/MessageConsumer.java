package com.bs.kafka;

import com.bs.dtos.CompanyDto;
import com.bs.services.CompanyService;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class MessageConsumer {

    Logger logger = LoggerFactory.getLogger(MessageConsumer.class);

    @Autowired
    private CompanyService companyService;

    @Autowired
    private Gson gson;

    @KafkaListener(topics = "${spring.kafka.topic}", groupId = "${spring.kafka.consumer.group-id}")
    public void consume(CompanyDto message) {
        logger.info("MESSAGE RECEIVED AT CONSUMER END -> " + message.toString());

        Long companyId = companyService.createCompany(message);
        logger.info("Company Created with id: {}" + companyId);
    }

}
