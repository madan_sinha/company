package com.bs.kafka;

import com.bs.dtos.CompanyDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class MessageProducer {

    Logger logger = LoggerFactory.getLogger(MessageProducer.class);

    @Value("${spring.kafka.topic}")
    private String topic;

    @Autowired
    @Qualifier(value = "domainEventKafkaTemplate")
    private KafkaTemplate<String, Object> kafkaTemplate;

    public void sendMessage(CompanyDto message) {
        logger.info("MESSAGE SENT FROM PRODUCER END -> " + message);
        kafkaTemplate.send(topic, message);
    }
}
