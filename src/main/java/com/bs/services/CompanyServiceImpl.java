package com.bs.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.bs.dtos.CompanyDto;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bs.entities.Company;
import com.bs.repositories.CompanyRepository;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CompanyServiceImpl implements CompanyService {
	
	@Autowired
	private CompanyRepository companyRepository;

	@Override
	public Long createCompany(CompanyDto companyDto) {
		Company company = new Company();
		company.setCode(companyDto.getCompanyCode());
		company.setName(companyDto.getCompanyName());
		company.setDescription(companyDto.getCompanyDescription());

		company = companyRepository.save(company);
		return company.getId();
	}

	@Override
	public CompanyDto findCompany(Long id) {
		Company company = companyRepository.getCompanyById(id);
		CompanyDto companyDto = null;
		if(company != null) {
			companyDto = getCompanyDto(company);
		}
		return companyDto;
	}

	private CompanyDto getCompanyDto(Company company) {
		CompanyDto companyDto = new CompanyDto();
		companyDto.setCompanyId(company.getId());
		companyDto.setCompanyCode(company.getCode());
		companyDto.setCompanyName(company.getName());
		companyDto.setCompanyDescription(company.getDescription());
		return companyDto;
	}

	@Override
	public List<CompanyDto> findAll() {
		List<Company> companyList=companyRepository.findAll();
		return getCompanyDtos(companyList);
	}

	@NotNull
	private List<CompanyDto> getCompanyDtos(List<Company> companyList) {
		List<CompanyDto> companyDtoList = new ArrayList<>();
		if(companyList != null && !companyList.isEmpty()) {
			companyDtoList = companyList
					.stream()
					.map(company -> getCompanyDto(company))
					.collect(Collectors.toList());
		}
		return companyDtoList;
	}

	@Override
	public List<CompanyDto> findCompaniesBySupplierIds(Long supplierId) {
		List<Company> companyList = companyRepository.findCompaniesBySupplierIds(supplierId);

		return getCompanyDtos(companyList);
	}

}
