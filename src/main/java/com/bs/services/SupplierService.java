package com.bs.services;

import com.bs.dtos.SupplierDto;

import java.util.List;

public interface SupplierService {

    SupplierDto findSupplierById(Long supplierId);

    Long createSupplier(SupplierDto supplierDto);

    List<SupplierDto> findSuppliersByCompanyId(Long companyId);
}
