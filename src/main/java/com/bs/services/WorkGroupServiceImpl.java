package com.bs.services;

import com.bs.dtos.WorkGroupDto;
import com.bs.entities.WorkGroup;
import com.bs.repositories.WorkGroupRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class WorkGroupServiceImpl implements WorkGroupService {

    Logger logger = LoggerFactory.getLogger(WorkGroupServiceImpl.class);

    @Autowired
    private WorkGroupRepository workGroupRepository;

    @Override
    public WorkGroupDto findWorkGroup(Long groupId) {
        Optional<WorkGroup> workGroup = workGroupRepository.findById(groupId);
        WorkGroupDto workGroupDto = null;
        if (workGroup.isPresent()) {
            workGroupDto = getWorkGroupDto(workGroup.get());
        }
        return workGroupDto;
    }

    public List<WorkGroup> findWorkGroupsByIDs(List<Long> groupIds) {
        List<WorkGroup> workGroupList = workGroupRepository.findAllById(groupIds);
        return workGroupList;
    }

    @Override
    public List<WorkGroupDto> getWorkGroupsByCompanyId(Long companyId) {
        List<WorkGroup> workGroups = workGroupRepository.findAllByCompanyId(companyId);

        List<WorkGroupDto> workGroupDtoList = workGroups.stream().map(workGroup -> getWorkGroupDto(workGroup)).collect(Collectors.toList());
        return workGroupDtoList;
    }

    @Override
    public Long createWorkGroup(WorkGroupDto workGroupDto) {
        WorkGroup workGroup = null;
        try {
            workGroup = getWorkGroup(workGroupDto);
            workGroup = workGroupRepository.save(workGroup);
        } catch (Exception ex) {
            logger.error("Not able to save workgroup ", ex);
            throw ex;
        }
        return workGroup.getId();
    }

    private WorkGroup getWorkGroup(WorkGroupDto workGroupDto) {
        WorkGroup workGroup = new WorkGroup();
        workGroup.setCode(workGroupDto.getCode());
        workGroup.setName(workGroupDto.getGroupName());
        workGroup.setDescription(workGroupDto.getDescription());
        workGroup.setCompanyId(workGroupDto.getCompanyId());
        return workGroup;
    }

    private WorkGroupDto getWorkGroupDto(WorkGroup workGroup) {
        WorkGroupDto workGroupDto = new WorkGroupDto();
        workGroupDto.setId(workGroup.getId());
        workGroupDto.setGroupName(workGroup.getName());
        workGroupDto.setCode(workGroup.getCode());
        workGroupDto.setDescription(workGroup.getDescription());
        workGroupDto.setCompanyId(workGroup.getCompanyId());
        return workGroupDto;
    }
}
