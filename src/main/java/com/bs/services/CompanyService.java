package com.bs.services;

import java.util.List;

import com.bs.dtos.CompanyDto;
import com.bs.entities.Company;

public interface CompanyService {

	public Long createCompany(CompanyDto companyDto);

	public CompanyDto findCompany(Long id);

	public List<CompanyDto> findAll();

	List<CompanyDto> findCompaniesBySupplierIds(Long supplierId);
}
