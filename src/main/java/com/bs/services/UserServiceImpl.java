package com.bs.services;

import com.bs.dtos.UserDto;
import com.bs.entities.User;
import com.bs.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private WorkGroupService workGroupService;

    @Autowired
    private UserRepository userRepository;


    @Override
    public Long createUser(UserDto userDto) {
        User user = new User();
        try {
            user.setName(userDto.getName());
            user.setEmail(userDto.getEmail());
            user.setWorkGroups(workGroupService.findWorkGroupsByIDs(userDto.getWorkGroupsIds()));
            Long userId = userRepository.sequenceForUser() !=null ? userRepository.sequenceForUser() : 0l;
            user.setId(userId+1);

            user = userRepository.save(user);
        } catch(Exception ex) {
            logger.error("Not able to save User to DB", ex);
        }
        return user.getId();
    }

    @Override
    public UserDto getUserById(Long userId) {
        Optional<User> user = userRepository.findById(userId);
        UserDto userDto = null;
        if(user.isPresent()) {
            userDto = getUserDto(user.get());
        }
        return userDto;
    }

    @Override
    public List<UserDto> getUsersByWorkGroupId(Long workgroupId) {
        List<User> users = userRepository.findAllByWorkGroups(workgroupId);

        List<UserDto> userDtoList = users.stream().map(user -> getUserDto(user)).collect(Collectors.toList());
        return userDtoList;
    }

    @Override
    public List<UserDto> getUsersByCompanyId(Long companyId) {
        List<UserDto> userDtoList = null;
        try {
            List<User> users = userRepository.findAllUsersByCompanyId(companyId);
            userDtoList = users.stream().map(user -> getUserDto(user)).collect(Collectors.toList());
        } catch(Exception ex) {
            logger.error("Not able to fet users for Company ", ex);
        }

        return userDtoList;
    }

    private UserDto getUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setEmail(user.getEmail());
        userDto.setWorkGroupsIds(user.getWorkGroups().stream().map(w -> w.getId()).collect(Collectors.toList()));
        return userDto;
    }
}
