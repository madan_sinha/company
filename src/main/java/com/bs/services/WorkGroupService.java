package com.bs.services;

import com.bs.dtos.UserDto;
import com.bs.dtos.WorkGroupDto;
import com.bs.entities.WorkGroup;

import java.util.List;

public interface WorkGroupService {
    WorkGroupDto findWorkGroup(Long anyLong);

    Long createWorkGroup(WorkGroupDto workGroupDto);

    public List<WorkGroup> findWorkGroupsByIDs(List<Long> groupIds);

    List<WorkGroupDto> getWorkGroupsByCompanyId(Long companyId);
}
