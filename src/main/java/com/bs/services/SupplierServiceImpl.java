package com.bs.services;

import com.bs.dtos.SupplierDto;
import com.bs.entities.Company;
import com.bs.entities.Supplier;
import com.bs.repositories.CompanyRepository;
import com.bs.repositories.SupplierRepository;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class SupplierServiceImpl implements SupplierService {

    Logger logger = LoggerFactory.getLogger(SupplierServiceImpl.class);

    @Autowired
    private SupplierRepository supplierRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public SupplierDto findSupplierById(Long supplierId) {
        Optional<Supplier> supplierOptional = supplierRepository.findById(supplierId);
        SupplierDto supplierDto = null;
        if(supplierOptional.isPresent()) {
            Supplier supplier = supplierOptional.get();
            supplierDto = getSupplierDto(supplier);
        }
        return supplierDto;
    }

    @NotNull
    private SupplierDto getSupplierDto(Supplier supplier) {

        SupplierDto supplierDto = new SupplierDto();
        supplierDto.setId(supplier.getId());
        supplierDto.setName(supplier.getName());
        supplierDto.setAddress(supplier.getAddress());
        supplierDto.setCompanyIds(supplier.getCompanies().stream().map(company -> company.getId()).collect(Collectors.toList()));
        return supplierDto;
    }

    @Override
    public Long createSupplier(SupplierDto supplierDto) {
        Supplier supplier = getSupplierFromSupplierDto(supplierDto);
        try {
            List<Company> companyList = companyRepository.findAllById(supplierDto.getCompanyIds());
            supplier.setCompanies(companyList);
            Long supplierId = supplierRepository.sequenceForSupplier() !=null ? supplierRepository.sequenceForSupplier() : 0l;
            supplier.setId(supplierId+1);
            supplier = supplierRepository.save(supplier);
        } catch (Exception e) {
            logger.error("Not able to save Supplier to DB", e);
            throw e;
        }
        return supplier.getId();
    }

    private Supplier getSupplierFromSupplierDto(SupplierDto supplierDto) {
        Supplier supplier = new Supplier();
        supplier.setName(supplierDto.getName());
        supplier.setAddress(supplierDto.getAddress());
        supplier.setGis(supplierDto.getGis());
        return supplier;
    }

    @Override
    public List<SupplierDto> findSuppliersByCompanyId(Long companyId) {
        List<SupplierDto> supplierDtoList = null;
        try {
            List<Supplier> supplierList = supplierRepository.findSuppliersByCompanyId(companyId);

            supplierDtoList = supplierList.stream().map(supplier -> getSupplierDto(supplier)).collect(Collectors.toList());
        } catch(Exception e) {
            logger.error("Error while fetching Suppliers by companyIds ", e);
            throw e;
        }
        return supplierDtoList;
    }
}
