package com.bs.services;

import com.bs.dtos.UserDto;

import java.util.List;

public interface UserService {
    Long createUser(UserDto userDto);

    UserDto getUserById(Long id);

    List<UserDto> getUsersByWorkGroupId(Long workgroupId);

    List<UserDto> getUsersByCompanyId(Long companyId);
}
