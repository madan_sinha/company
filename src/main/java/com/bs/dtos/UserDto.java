package com.bs.dtos;

import com.bs.entities.WorkGroup;
import lombok.*;

import java.util.List;

@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

    private Long id;
    private String name;
    private String email;

    @NonNull
    private List<Long> workGroupsIds;

    public UserDto(String name, String email, List<Long> workGroupsIds) {
        this.name = name;
        this.email = email;
        this.workGroupsIds = workGroupsIds;
    }

}
