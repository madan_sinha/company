package com.bs.dtos;

import lombok.Setter;
import lombok.Getter;
import lombok.ToString;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;


@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SupplierDto {

    private Long id;
    private String name;
    private String address;
    private String gis;
    private List<Long> companyIds;

    public SupplierDto(String name, String address, String gis, List<Long> companyIds) {
        this.name = name;
        this.address = address;
        this.gis = gis;
        this.companyIds = companyIds;
    }

}
