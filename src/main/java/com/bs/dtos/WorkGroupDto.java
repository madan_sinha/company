package com.bs.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WorkGroupDto {

    private Long id;

    private String groupName;

    private String code;

    private String description;

    private Long companyId;

    public WorkGroupDto(String groupName, String code, String description, Long companyId) {
        this.code = code;
        this.groupName = groupName;
        this.description = description;
        this.companyId = companyId;
    }
}
