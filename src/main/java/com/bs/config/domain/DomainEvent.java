package com.bs.config.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DomainEvent<T> {

    private T payload;

    private String id;

    private Map<String, String> headers;
}
