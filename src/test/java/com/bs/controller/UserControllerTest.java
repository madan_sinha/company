package com.bs.controller;

import com.bs.dtos.UserDto;
import com.bs.services.UserService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = UserController.class)
public class UserControllerTest {

    @MockBean
    private UserService userService;
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private Gson gson;

    @Test
    public void test_shouldReturnUserDetails() throws Exception {
        //Given
        List<Long> workGroupIds = new ArrayList<>();
        workGroupIds.add(1l);
        UserDto userDto = new UserDto(1l, "Brightly User", "Brightly@brightlysoftware.com", workGroupIds);

        BDDMockito.given(userService.getUserById(ArgumentMatchers.anyLong())).willReturn(userDto);

        //Action
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/user/get/1"))
                .andReturn();
        //Assert
        Assertions.assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());

        String response = result.getResponse().getContentAsString();
        UserDto responseDto = gson.fromJson(response, UserDto.class);
        Assertions.assertThat(responseDto.getName()).isEqualTo("Brightly User");
    }

    @Test
    public void test_shouldCreateNewUser() throws Exception {
        //Given
        List<Long> workGroupIds = new ArrayList<>();
        workGroupIds.add(1l);
        UserDto userDto = new UserDto(1l, "Brightly User", "Brightly@brightlysoftware.com", workGroupIds);

        //When
        Mockito.when(userService.createUser(userDto)).thenReturn(1l);
        MockHttpServletRequestBuilder mockRequest=MockMvcRequestBuilders.post("/user/create")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(gson.toJson(userDto));
        //Then
        mockMvc.perform(mockRequest)
                .andExpect(status().isOk());
    }

    @Test
    public void test_shouldReturnAllUsersByWorkGroupId() throws Exception {
        //Given
        List<Long> workGroupIds = new ArrayList<>();
        workGroupIds.add(1l);
        UserDto userDto1 = new UserDto(1l, "Admin User", "Brightly_admin@brightlysoftware.com", workGroupIds);
        UserDto userDto2 = new UserDto(2l, "Super User", "Brightly_super@brightlysoftware.com", workGroupIds);

        List<UserDto> userDtoList = new ArrayList<>();
        userDtoList.add(userDto1);
        userDtoList.add(userDto2);

        BDDMockito.given(userService.getUsersByWorkGroupId(ArgumentMatchers.anyLong())).willReturn(userDtoList);

        //Action
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/user/getByWorkGroup/1"))
                .andReturn();
        //Assert
        Assertions.assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());

        String response = result.getResponse().getContentAsString();

        Type listType = new TypeToken<ArrayList<UserDto>>(){}.getType();
        List<UserDto> responseDto = gson.fromJson(response, listType);
        Assertions.assertThat(responseDto.get(0).getName()).isEqualTo("Admin User");
        Assertions.assertThat(responseDto.get(1).getName()).isEqualTo("Super User");
    }

    @Test
    public void test_shouldReturnAllUsersByCompanyId() throws Exception {
        //Given
        List<Long> workGroupIds1 = new ArrayList<>();
        workGroupIds1.add(1l);
        UserDto userDto1 = new UserDto(1l, "Admin User", "Brightly_admin@brightlysoftware.com", workGroupIds1);

        List<Long> workGroupIds2 = new ArrayList<>();
        workGroupIds2.add(2l);
        UserDto userDto2 = new UserDto(2l, "Super User", "Brightly_super@brightlysoftware.com", workGroupIds2);

        List<UserDto> companyUserDtoList = new ArrayList<>();
        companyUserDtoList.add(userDto1);
        companyUserDtoList.add(userDto2);

        BDDMockito.given(userService.getUsersByCompanyId(ArgumentMatchers.anyLong())).willReturn(companyUserDtoList);

        //Action
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/user/getByCompany/1"))
                .andReturn();
        //Assert
        Assertions.assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());

        String response = result.getResponse().getContentAsString();

        Type listType = new TypeToken<ArrayList<UserDto>>(){}.getType();
        List<UserDto> responseDto = gson.fromJson(response, listType);
        Assertions.assertThat(responseDto.get(0).getName()).isEqualTo("Admin User");
        Assertions.assertThat(responseDto.get(1).getName()).isEqualTo("Super User");
    }
}
