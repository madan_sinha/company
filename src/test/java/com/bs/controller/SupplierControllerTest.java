package com.bs.controller;

import com.bs.dtos.SupplierDto;
import com.bs.services.SupplierService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = SupplierController.class)
class SupplierControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SupplierService supplierService;

    @Autowired
    private Gson gson;

    @Test
    public void test_shouldCreateNewSupplier() throws Exception {

        List<Long> companyIds = new ArrayList<>();
        companyIds.add(1l);
        //Given
        SupplierDto supplierDto = new SupplierDto("Dell", "Dell Software, Noida Sector 16, UP, India", "GIS", companyIds);

        //When
        Mockito.when(supplierService.createSupplier(supplierDto)).thenReturn(1l);
        MockHttpServletRequestBuilder mockRequest=MockMvcRequestBuilders.post("/supplier/create")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(gson.toJson(supplierDto));
        //Then
        mockMvc.perform(mockRequest)
                .andExpect(status().isOk());

    }

    @Test
    public void test_shouldReturnSupplierDetails() throws Exception {

        List<Long> companyIds = new ArrayList<>();
        companyIds.add(1l);

        //Given
        SupplierDto supplierDto = new SupplierDto("Dell", "Dell Software, Noida Sector 16, UP, India", "GIS", companyIds);
        BDDMockito.given(supplierService.findSupplierById(ArgumentMatchers.anyLong())).willReturn(supplierDto);

        //Action
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/supplier/get/1"))
                .andReturn();
        //Assert
        Assertions.assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());

        String response = result.getResponse().getContentAsString();
        SupplierDto responseDto = gson.fromJson(response, SupplierDto.class);
        Assertions.assertThat(responseDto.getName()).isEqualTo("Dell");
    }

    @Test
    public void test_shouldReturnSupplierDetailsFromCompanyIds() throws Exception {

        List<Long> companyIds = new ArrayList<>();
        companyIds.add(1l);

        //Given
        SupplierDto supplierDto1 = new SupplierDto("Dell", "Dell Software, Noida Sector 16, UP, India", "GIS", companyIds);
        SupplierDto supplierDto2 = new SupplierDto("Hp", "Hp Software, Noida Sector 16, UP, India", "GIS", companyIds);

        List<SupplierDto> supplierDtoList = new ArrayList<>();
        supplierDtoList.add(supplierDto1);
        supplierDtoList.add(supplierDto2);

        BDDMockito.given(supplierService.findSuppliersByCompanyId(ArgumentMatchers.anyLong())).willReturn(supplierDtoList);

        //Action
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/supplier/getByCompany/1"))
                .andReturn();
        //Assert
        Assertions.assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());

        String response = result.getResponse().getContentAsString();
        Type listType = new TypeToken<ArrayList<SupplierDto>>(){}.getType();
        List<SupplierDto> responseDto = gson.fromJson(response, listType);
        Assertions.assertThat(responseDto.get(0).getName()).isEqualTo("Dell");
        Assertions.assertThat(responseDto.get(1).getName()).isEqualTo("Hp");
    }

}