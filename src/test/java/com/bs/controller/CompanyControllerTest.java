package com.bs.controller;

import com.bs.dtos.CompanyDto;
import com.bs.kafka.MessageProducer;
import com.bs.services.CompanyService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = CompanyController.class)
class CompanyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CompanyService companyService;

    @MockBean
    private MessageProducer messageProducer;

    @Autowired
    private Gson gson;


    @Test
    public void test_shouldReturnCompanyDetails() throws Exception {

        //Given
        CompanyDto companyDto = new CompanyDto(1l, "Brightly", "Brightly", "Brightly Software India pvt. ltd.");
        BDDMockito.given(companyService.findCompany(ArgumentMatchers.anyLong())).willReturn(companyDto);

        //Action
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/company/get/1"))
                .andReturn();
        //Assert
        Assertions.assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());

        String response = result.getResponse().getContentAsString();
        CompanyDto responseDto = gson.fromJson(response, CompanyDto.class);
        Assertions.assertThat(responseDto.getCompanyCode()).isEqualTo("Brightly");
    }
    
    @Test
    public void test_shouldCreateNewCompany() throws Exception {
    	//Given 
    	 CompanyDto companyDto = new CompanyDto(2l, "Brightly", "Brightly", "Brightly Software India pvt. ltd.");
    	
    	 //When
    	 Mockito.when(companyService.createCompany(companyDto)).thenReturn(2l); 	 
    	 MockHttpServletRequestBuilder mockRequest=MockMvcRequestBuilders.post("/company/create")
    			 .contentType(MediaType.APPLICATION_JSON)
    			 .accept(MediaType.APPLICATION_JSON)
    			 .content(gson.toJson(companyDto));
    	 //Then
    	 mockMvc.perform(mockRequest)
    	 		.andExpect(status().isOk());
    	
    }
    
    @Test
    public void test_shouldReturnAllCompany() throws Exception {
    	//Given   	
	   	 CompanyDto dto = new CompanyDto(1l, "Brightly", "Brightly", "Brightly Software India pvt. ltd.");
	   	 CompanyDto dto2 = new CompanyDto(2l, "TCS", "TCS", "TCS  India pvt. ltd.");
	   	 List<CompanyDto> dtoList= new ArrayList<CompanyDto>();
	   				dtoList.add(dto);
	   				dtoList.add(dto2);
	   	 BDDMockito.given(companyService.findAll()).willReturn(dtoList);

	   	 //When
		 MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/company/all"))
		             .andReturn();
   	 
	   	 //Then
		  Assertions.assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());	       
		  String response = result.getResponse().getContentAsString();
		  Type listType = new TypeToken<ArrayList<CompanyDto>>(){}.getType();
		  List<CompanyDto> responseDto = gson.fromJson(response, listType);
		  Assertions.assertThat(responseDto.get(1).getCompanyCode()).isEqualTo("TCS");    	
    }
}