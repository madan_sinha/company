package com.bs.controller;

import com.bs.dtos.CompanyDto;
import com.bs.dtos.WorkGroupDto;
import com.bs.services.WorkGroupService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = WorkGroupController.class)
public class WorkGroupControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private WorkGroupService workGroupService;
    @Autowired
    private Gson gson;

    @Test
    public void test_shouldReturnWorkGroupDetails() throws Exception {

        //Given
        WorkGroupDto workGroupDto = new WorkGroupDto(1l, "Brightly Engineering", "Engineering_India", "Brightly Engineering India Group", 1l);
        BDDMockito.given(workGroupService.findWorkGroup(ArgumentMatchers.anyLong())).willReturn(workGroupDto);

        //Action
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/workgroup/get/1"))
                .andReturn();
        //Assert
        Assertions.assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());

        String response = result.getResponse().getContentAsString();
        WorkGroupDto responseDto = gson.fromJson(response, WorkGroupDto.class);
        Assertions.assertThat(responseDto.getCode()).isEqualTo("Engineering_India");
    }

    @Test
    public void test_shouldCreateWorkGroupDetails() throws Exception {

        //Given
        WorkGroupDto workGroupDto = new WorkGroupDto("Brightly Engineering", "Engineering_India", "Brightly Engineering India Group", 1l);
        BDDMockito.given(workGroupService.createWorkGroup(workGroupDto)).willReturn(1l);

        //Action
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/workgroup/create")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(gson.toJson(workGroupDto)))
                .andReturn();
        //Assert
        Assertions.assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());

        String response = result.getResponse().getContentAsString();

        Assertions.assertThat(response).isEqualTo("1");
    }

    @Test
    public void test_shouldReturnWorkGroupDetailsByCompanyId() throws Exception {

        //Given
        WorkGroupDto workGroupDto1 = new WorkGroupDto(1l, "Brightly Engineering", "Engineering_India", "Brightly Engineering India Group", 1l);
        WorkGroupDto workGroupDto2 = new WorkGroupDto(2l, "Brightly Operations", "Operations_India", "Brightly Operations India Group", 2l);

        List<WorkGroupDto> workGroupDtoList = new ArrayList<>();
        workGroupDtoList.add(workGroupDto1);
        workGroupDtoList.add(workGroupDto2);

        BDDMockito.given(workGroupService.getWorkGroupsByCompanyId(ArgumentMatchers.anyLong())).willReturn(workGroupDtoList);

        //Action
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/workgroup/getByCompany/1"))
                .andReturn();
        //Assert
        Assertions.assertThat(result.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());

        String response = result.getResponse().getContentAsString();
        Type listType = new TypeToken<ArrayList<WorkGroupDto>>(){}.getType();
        List<WorkGroupDto> responseDto = gson.fromJson(response, listType);
        Assertions.assertThat(responseDto.get(0).getCode()).isEqualTo("Engineering_India");
        Assertions.assertThat(responseDto.get(1).getCode()).isEqualTo("Operations_India");
    }

}
