package com.bs.services;

import com.bs.dtos.WorkGroupDto;
import com.bs.entities.WorkGroup;
import com.bs.repositories.WorkGroupRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
public class WorkGroupServiceImplTest {

    @Mock
    private WorkGroupRepository workGroupRepository;

    @InjectMocks
    private WorkGroupServiceImpl workGroupService;

    @Test
    public void test_shouldCreateWorkGroup() {
        //Given
        WorkGroupDto workGroupDto = preparedDto();
        WorkGroup workGroup = new WorkGroup();
        workGroup.setId(1l);
        workGroup.setCode(workGroupDto.getCode());
        workGroup.setName(workGroupDto.getGroupName());
        workGroup.setDescription(workGroupDto.getDescription());
        workGroup.setCompanyId(workGroupDto.getCompanyId());

        //When
        BDDMockito.given(workGroupRepository.save(Mockito.any(WorkGroup.class))).willReturn(workGroup);
        //Then
        Long workgroupId = workGroupService.createWorkGroup(workGroupDto);
        assertEquals(workgroupId, 1l);


    }

    @Test
    public void test_shouldFindWorkGroupDetails() {
        //Given
        WorkGroupDto workGroupDto = preparedDto();
        WorkGroup workGroup = new WorkGroup();
        workGroup.setId(1l);
        workGroup.setCode(workGroupDto.getCode());
        workGroup.setName(workGroupDto.getGroupName());
        workGroup.setDescription(workGroupDto.getDescription());
        workGroup.setCompanyId(workGroupDto.getCompanyId());
        BDDMockito.given(workGroupRepository.findById(workGroupDto.getId())).willReturn(Optional.of(workGroup));

        //When
        WorkGroupDto responseDto = workGroupService.findWorkGroup(workGroupDto.getId());
        //Then
        Assertions.assertThat(responseDto.getCode()).isEqualTo("Engineering_India");

    }

    @Test
    public void test_shouldReturnWorkGroupsByCompanyId() {
        //Given
        WorkGroupDto workGroupDto = preparedDto();
        WorkGroup workGroup = new WorkGroup();
        workGroup.setId(1l);
        workGroup.setCode(workGroupDto.getCode());
        workGroup.setName(workGroupDto.getGroupName());
        workGroup.setDescription(workGroupDto.getDescription());
        workGroup.setCompanyId(workGroupDto.getCompanyId());

        List<WorkGroup> workGroupList = new ArrayList<WorkGroup>();
        workGroupList.add(workGroup);
        BDDMockito.given(workGroupRepository.findAllByCompanyId(workGroupDto.getId())).willReturn(workGroupList);

        //When
        List<WorkGroupDto> responseDto = workGroupService.getWorkGroupsByCompanyId(workGroupDto.getId());
        //Then
        Assertions.assertThat(responseDto.get(0).getCode()).isEqualTo("Engineering_India");
    }

    private WorkGroupDto preparedDto() {
        WorkGroupDto workGroupDto = new WorkGroupDto(1l, "Brightly Engineering", "Engineering_India", "Brightly Engineering India Group", 1l);
        return workGroupDto;
    }

}
