package com.bs.services;

import com.bs.dtos.UserDto;
import com.bs.dtos.WorkGroupDto;
import com.bs.entities.User;
import com.bs.entities.WorkGroup;
import com.bs.repositories.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
public class UserServiceImplTest {


    @Mock
    private WorkGroupService workGroupService;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void test_shouldCreateUser() {
        //Given
        UserDto userDto = preparedDto();
        List<WorkGroup> workGroupList = new ArrayList<>();
        workGroupList.add(preparedWorkGroup());
        BDDMockito.given(workGroupService.findWorkGroupsByIDs(userDto.getWorkGroupsIds())).willReturn(workGroupList);
        User user = new User();
        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        user.setWorkGroups(workGroupService.findWorkGroupsByIDs(userDto.getWorkGroupsIds()));
        Long userId = userRepository.sequenceForUser() != null ? userRepository.sequenceForUser() : 0l;
        user.setId(userId + 1);

        //When
        BDDMockito.given(userRepository.save(Mockito.any(User.class))).willReturn(user);
        //Then
        Long newUserId = userService.createUser(userDto);
        assertEquals(newUserId, 1l);
    }

    @Test
    public void test_shouldGetUserById() {
        //Given
        UserDto userDto = preparedDto();
        User user = new User();
        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        //When
        BDDMockito.given(userRepository.findById(1l)).willReturn(Optional.of(user));
        //Then
        userDto = userService.getUserById(1l);
        Assertions.assertThat(userDto.getEmail()).isEqualTo("Brightly@brightlysoftware.com");
    }

    @Test
    public void test_shouldGetUsersByWorkGroupId() {
        //Given
        UserDto userDto = preparedDto();
        User user = new User();
        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        //When
        List<User> userList = new ArrayList<>();
        userList.add(user);
        BDDMockito.given(userRepository.findAllByWorkGroups(1l)).willReturn(userList);
        //Then
        List<UserDto> userDtoList = userService.getUsersByWorkGroupId(1l);
        Assertions.assertThat(userDtoList.get(0).getEmail()).isEqualTo("Brightly@brightlysoftware.com");
    }

    @Test
    public void test_shouldGetUsersByCompanyId() {
        //Given
        UserDto userDto = preparedDto();
        User user = new User();
        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        //When
        List<User> userList = new ArrayList<>();
        userList.add(user);
        BDDMockito.given(userRepository.findAllUsersByCompanyId(1l)).willReturn(userList);
        //Then
        List<UserDto> userDtoList = userService.getUsersByCompanyId(1l);
        Assertions.assertThat(userDtoList.get(0).getEmail()).isEqualTo("Brightly@brightlysoftware.com");
    }

    private UserDto preparedDto() {
        List<Long> workGroupIds = new ArrayList<>();

        workGroupIds.add(1l);
        UserDto userDto = new UserDto(1l, "Brightly User", "Brightly@brightlysoftware.com", workGroupIds);

        return userDto;
    }

    private WorkGroup preparedWorkGroup() {
        WorkGroup workGroup = new WorkGroup();
        workGroup.setId(1l);
        workGroup.setCode("Engineering_India");
        workGroup.setName("Brightly Engineering");
        workGroup.setDescription("Brightly Engineering India Group");
        workGroup.setCompanyId(1l);
        return workGroup;
    }
}
