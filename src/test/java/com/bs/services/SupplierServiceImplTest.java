package com.bs.services;


import com.bs.dtos.CompanyDto;
import com.bs.dtos.SupplierDto;
import com.bs.entities.Company;
import com.bs.entities.Supplier;
import com.bs.repositories.CompanyRepository;
import com.bs.repositories.SupplierRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
public class SupplierServiceImplTest {
    @InjectMocks
    private SupplierServiceImpl supplierService;

    @Mock
    private SupplierRepository supplierRepository;

    @Mock
    private CompanyRepository companyRepository;

    @Test
    public void test_shouldCreateSupplier() {
        SupplierDto supplierDto = preparedDto();
        List<Company> companyList = new ArrayList<>();
        companyList.add(preparedCompanyDto());
        Supplier supplier = preparedSupplier();
        //Given
        BDDMockito.given(companyRepository.findAllById(supplierDto.getCompanyIds())).willReturn(companyList);
        BDDMockito.given(supplierRepository.save(Mockito.any(Supplier.class))).willReturn(supplier);

        //When
        Long supplierId = supplierService.createSupplier(supplierDto);
        //Then
        assertEquals(supplier.getId(), supplierId);
    }

    @Test
    public void test_shouldFindSupplierById() {
        // Given
        BDDMockito.given(supplierRepository.findById(1l)).willReturn(Optional.of(preparedSupplier()));
        // When
        SupplierDto responseDto = supplierService.findSupplierById(1l);
        // Then
        Assertions.assertThat(responseDto.getName()).isEqualTo("Dell");
    }

    @Test
    public void test_shouldFindSuppliersByCompanyId() {
        List<Supplier> supplierList = new ArrayList<>();
        supplierList.add(preparedSupplier());
        //Given
        BDDMockito.given(supplierRepository.findSuppliersByCompanyId(1l)).willReturn((supplierList));
        //When
        List<SupplierDto> responseDtoList = supplierService.findSuppliersByCompanyId(1l);
        // Then
        Assertions.assertThat(responseDtoList.get(0).getName()).isEqualTo("Dell");
    }

    private SupplierDto preparedDto() {
        List<Long> companyIds = new ArrayList<>();
        companyIds.add(1l);
        SupplierDto supplierDto = new SupplierDto("Dell", "Dell Software, Noida Sector 16", "gis", companyIds);
        return supplierDto;
    }

    private Supplier preparedSupplier() {
        SupplierDto supplierDto = preparedDto();
        Supplier supplier = new Supplier();
        supplier.setName(supplierDto.getName());
        supplier.setAddress(supplierDto.getAddress());
        supplier.setGis(supplierDto.getGis());
        supplier.setId(1l);
        return supplier;
    }

    private Company preparedCompanyDto() {
        CompanyDto companyDto = new CompanyDto(5l, "Brightly", "Brightly", "Brightly Software India pvt. ltd.");
        Company company = new Company();
        company.setId(companyDto.getCompanyId());
        company.setCode(companyDto.getCompanyCode());
        company.setName(companyDto.getCompanyName());
        company.setDescription(companyDto.getCompanyDescription());
        return company;

    }
}
