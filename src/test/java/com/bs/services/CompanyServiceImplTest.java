package com.bs.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.bs.dtos.CompanyDto;
import com.bs.entities.Company;
import com.bs.repositories.CompanyRepository;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class CompanyServiceImplTest {

	@Mock
	private CompanyRepository companyRepository;

	@InjectMocks
	private CompanyServiceImpl companyService;

	@Test
	public void test_shouldCreateCompany() throws Exception {
		// Given
		CompanyDto companyDto = preparedDto();
		Company company = new Company();
		company.setId(companyDto.getCompanyId());
		company.setCode(companyDto.getCompanyCode());
		company.setName(companyDto.getCompanyName());
		company.setDescription(companyDto.getCompanyDescription());

		BDDMockito.given(companyRepository.save(Mockito.any(Company.class))).willReturn(company);
		// when(companyRepository.save(Mockito.any(Company.class))).thenReturn(company);

		Long companyId = companyService.createCompany(companyDto);
		assertEquals(companyDto.getCompanyId(), companyId);
	}

	@Test
	public void test_shouldFindTheCompany() throws Exception {

		// Given
		CompanyDto companyDto = preparedDto();
		Company company = new Company();
		company.setId(companyDto.getCompanyId());

		company.setCode(companyDto.getCompanyCode());
		company.setName(companyDto.getCompanyName());
		company.setDescription(companyDto.getCompanyDescription());
		BDDMockito.given(companyRepository.getCompanyById(companyDto.getCompanyId())).willReturn(company);

		// When
		CompanyDto responseDto = companyService.findCompany(companyDto.getCompanyId());

		// Then
		Assertions.assertThat(responseDto.getCompanyCode()).isEqualTo("Brightly");
	}

	@Test
	public void test_shouldFindAllCompany() {
		// Given
		Company company = new Company();
		CompanyDto companyDto = preparedDto();
		company.setId(companyDto.getCompanyId());

		company.setCode(companyDto.getCompanyCode());
		company.setName(companyDto.getCompanyName());
		company.setDescription(companyDto.getCompanyDescription());
		List<Company> dtoList = new ArrayList<Company>();
		dtoList.add(company);

		BDDMockito.given(companyRepository.findAll()).willReturn(dtoList);
		List<CompanyDto> responseDto = companyService.findAll();

		Assertions.assertThat(responseDto.get(0).getCompanyCode()).isEqualTo("Brightly");

	}

	private CompanyDto preparedDto() {
		CompanyDto companyDto = new CompanyDto(5l, "Brightly", "Brightly", "Brightly Software India pvt. ltd.");

		return companyDto;

	}
}