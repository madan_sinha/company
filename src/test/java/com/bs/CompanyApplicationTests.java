package com.bs;

import com.bs.dtos.CompanyDto;
import com.bs.dtos.SupplierDto;
import com.bs.dtos.UserDto;
import com.bs.dtos.WorkGroupDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest(classes = CompanyApplication.class,
		webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CompanyApplicationTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	void contextLoads() {
	}

	@Test
	@Order(1)
	public void test_shouldCreateCompanyDetails() throws URISyntaxException {
		//arrange
		CompanyDto companyDto = new CompanyDto("Brightly", "Brightly", "Brightly Software India pvt. ltd.");
		URI uri = new URI("/company/create");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<CompanyDto> request = new HttpEntity<>(companyDto, headers);
		//act
		ResponseEntity<Long> response = restTemplate.postForEntity(uri, request, Long.class);
		//assert
		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		Assertions.assertThat(response.getBody().intValue()).isEqualTo(1);
	}

	@Test
	@Order(2)
	public void test_shouldGetCompanyDetails() {
		//arrange
		//act
		ResponseEntity<CompanyDto> response = restTemplate.getForEntity("/company/get/1", CompanyDto.class);
		//assert
		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		Assertions.assertThat(response.getBody().getCompanyCode()).isEqualTo("Brightly");
	}

	@Test
	@Order(3)
	public void test_shouldCreateWorkGroupDetails() throws URISyntaxException {
		//arrange
		WorkGroupDto workGroupDto = new WorkGroupDto("Brightly Engineering", "Engineering", "Brightly Engineering Group", 1l);
		URI uri = new URI("/workgroup/create");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<WorkGroupDto> request = new HttpEntity<>(workGroupDto, headers);
		//act
		ResponseEntity<Long> response = restTemplate.postForEntity(uri, request, Long.class);
		//assert
		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		Assertions.assertThat(response.getBody().intValue()).isEqualTo(1);
	}

	@Test
	@Order(4)
	public void test_shouldGetWorkGroupDetails() {
		//arrange
		//act
		ResponseEntity<WorkGroupDto> response = restTemplate.getForEntity("/workgroup/get/1", WorkGroupDto.class);
		//assert
		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		Assertions.assertThat(response.getBody().getCode()).isEqualTo("Engineering");
	}

	@Test
	@Order(5)
	public void test_shouldGetWorkGroupsByCompanyId() {
		//arrange
		//act
		Class<List<WorkGroupDto>> clas = (Class<List<WorkGroupDto>>)(Object)List.class;
		ResponseEntity<List<WorkGroupDto>> response = restTemplate.getForEntity("/workgroup/getByCompany/1", clas);
		//assert
		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		Assertions.assertThat(response.getBody().size()).isEqualTo(1);
	}

	@Test
	@Order(6)
	public void test_shouldCreateUserDetails() throws URISyntaxException {
		//arrange

		List<Long> workGroupsIds = new ArrayList<>();
		workGroupsIds.add(1l);

		UserDto userDto = new UserDto( "test_user", "test_user@brightlysoftware.com", workGroupsIds);
		URI uri = new URI("/user/create");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<UserDto> request = new HttpEntity<>(userDto, headers);
		//act
		ResponseEntity<Long> response = restTemplate.postForEntity(uri, request, Long.class);
		//assert
		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		Assertions.assertThat(response.getBody().intValue()).isEqualTo(1);
	}

	@Test
	@Order(7)
	public void test_shouldCreateUserDetailsV2() throws URISyntaxException {
		//arrange

		List<Long> workGroupsIds = new ArrayList<>();
		workGroupsIds.add(1l);

		UserDto userDto = new UserDto( "test_user", "test_user@brightlysoftware.com", workGroupsIds);
		URI uri = new URI("/user/create");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<UserDto> request = new HttpEntity<>(userDto, headers);
		//act
		ResponseEntity<Long> response = restTemplate.postForEntity(uri, request, Long.class);
		//assert
		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		Assertions.assertThat(response.getBody().intValue()).isEqualTo(2);
	}

	@Test
	@Order(8)
	public void test_shouldGetUserDetails() {
		//arrange
		//act
		ResponseEntity<UserDto> response = restTemplate.getForEntity("/user/get/1", UserDto.class);
		//assert
		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		Assertions.assertThat(response.getBody().getId().intValue()).isEqualTo(1);
	}

	@Test
	@Order(9)
	public void test_shouldGetUsersByWorkGroup() {
		//arrange
		//act
		Class<List<UserDto>> clas = (Class<List<UserDto>>)(Object)List.class;
		ResponseEntity<List<UserDto>> response = restTemplate.getForEntity("/user/getByWorkGroup/1", clas);
		//assert
		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		Assertions.assertThat(response.getBody().size()).isEqualTo(2);
	}

	@Test
	@Order(10)
	public void test_shouldGetAllUsersByCompany() {
		//arrange
		//act
		Class<List<UserDto>> clas = (Class<List<UserDto>>)(Object)List.class;
		ResponseEntity<List<UserDto>> response = restTemplate.getForEntity("/user/getByCompany/1", clas);
		//assert
		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		Assertions.assertThat(response.getBody().size()).isEqualTo(2);
	}

	@Test
	@Order(11)
	public void test_shouldCreateSupplierDetails() throws URISyntaxException {
		//arrange

		List<Long> companyIds = new ArrayList<>();
		companyIds.add(1l);

		SupplierDto supplierDto = new SupplierDto( "Dell", "Dell Software, Noida Sector 16", "gis", companyIds);
		URI uri = new URI("/supplier/create");
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<SupplierDto> request = new HttpEntity<>(supplierDto, headers);
		//act
		ResponseEntity<Long> response = restTemplate.postForEntity(uri, request, Long.class);
		//assert
		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		Assertions.assertThat(response.getBody().intValue()).isEqualTo(1);
	}

	@Test
	@Order(12)
	public void test_shouldGetSupplierDetails() {
		//arrange
		//act
		ResponseEntity<SupplierDto> response = restTemplate.getForEntity("/supplier/get/1", SupplierDto.class);
		//assert
		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		Assertions.assertThat(response.getBody().getId().intValue()).isEqualTo(1);
	}

	@Test
	@Order(13)
	public void test_shouldGetAllSupplierByCompanyId() {
		//arrange
		//act
		Class<List<SupplierDto>> clas = (Class<List<SupplierDto>>)(Object)List.class;
		ResponseEntity<List<SupplierDto>> response = restTemplate.getForEntity("/supplier/getByCompany/1", clas);
		//assert
		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		Assertions.assertThat(response.getBody().size()).isEqualTo(1);
	}

	@Test
	@Order(14)
	public void test_shouldGetAllCompaniesForSupplierId() {
		//arrange
		//act
		Class<List<CompanyDto>> clas = (Class<List<CompanyDto>>)(Object)List.class;
		ResponseEntity<List<CompanyDto>> response = restTemplate.getForEntity("/company/getBySupplier/1", clas);
		//assert
		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		Assertions.assertThat(response.getBody().size()).isEqualTo(1);
	}

}
